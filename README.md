# scli-doipjs

This is a Stateless Command Line Interface for the doip.js library.

[doip.js source code](https://codeberg.org/keyoxide/doipjs)

This SCLI is compatible with the Ariadne Implementation Test Suite ([source code](https://codeberg.org/keyoxide/ariadne-implementation-test-suite)).

## Usage

### verify-claim

To perform a basic identity claim verification:

```bash
# In a terminal
./scli-doipjs verify-claim https://fosstodon.org/@keyoxide 3637202523e7c1309ab79e99ef2dc5827b445f4b
# 2022-10-30T13:36:59.563Z https://fosstodon.org/@keyoxide 3637202523e7c1309ab79e99ef2dc5827b445f4b true
```